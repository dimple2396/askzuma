package Inheritance;

import org.testng.annotations.Test;



import org.testng.annotations.BeforeClass;

import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
public class Askzuma_Signin_parentclass {
	
	public WebDriver driver;
	
	@BeforeClass
	
	//parallel testng code
    @Parameters("browser")

	public void beforeTest(@Optional("firefox") String browser) throws Exception
	{
	if(browser.equalsIgnoreCase("chrome"))
	{
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\admin\\Desktop\\selenium webdrivers\\chromedriver.exe");
	ChromeOptions option=new ChromeOptions();
	option.addArguments("----disable-notification----");
	driver=new ChromeDriver(option);
	driver.manage().window().maximize();
	 }
	else if(browser.equalsIgnoreCase("firefox"))
	    {
		System.setProperty("webdriver.gecko.driver", "C:\\Users\\admin\\Desktop\\selenium webdrivers\\geckodriver.exe");
	FirefoxOptions option=new FirefoxOptions();
	option.addArguments("----disable-notification----");
	driver=new FirefoxDriver(option);
	driver.manage().window().maximize();
	}


	else
	{
	throw new Exception("Browser is not correct");
	}

	driver.manage().timeouts().implicitlyWait(2000, TimeUnit.SECONDS);

	}

	
	 @Test (priority = 0)
	  public void beforeClass() throws InterruptedException {
		
		  /*System.setProperty("webdriver.gecko.driver", "C:\\Users\\admin\\Desktop\\selenium webdrivers\\geckodriver.exe");
			 driver=new FirefoxDriver();
				
			  driver.manage().window().maximize();*/
			  driver.get("https://askzuma.com/");
			  Thread.sleep(5000);
			  
			  driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/ul[2]/li[1]/a")).click();
	}
			  
			  @Test (priority = 1)
			  public void bothvalueblank() throws InterruptedException {
					
					driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/div/div/form/footer/button")).click();
					Thread.sleep(5000);
					
					String actual_msg = driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/div/div/form/div[1]/div[1]/span")).getText();
					String expected_msg = "Required";
					Assert.assertEquals(actual_msg, expected_msg);
					
					String actual_msg1 = driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/div/div/form/div[1]/div[2]/span")).getText();
					String expected_msg1 = "Required";
					Assert.assertEquals(actual_msg1, expected_msg1);
			}
			  
			     @Test (priority = 2)
				  public void Emailblank() throws InterruptedException {
				
				    driver.findElement(By.id("Password")).sendKeys("123456");
				    driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/div/div/form/footer/button")).click();
				    Thread.sleep(5000);
				 
				   String actual_msg = driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/div/div/form/div[1]/div[1]/span")).getText();
				   String expected_msg = "Required";
				   Assert.assertEquals(actual_msg, expected_msg);
				 
				  }
				
				
				@Test (priority = 3)
				  public void PasswordBlank() throws InterruptedException {
					
					driver.findElement(By.id("Email")).sendKeys("pateldimple2323@gmail.com");
					driver.findElement(By.id("Password")).clear();
					driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/div/div/form/footer/button")).click();
					Thread.sleep(3000);
					
					String actual_msg = driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/div/div/form/div[1]/div[2]/span")).getText();
					String expected_msg = "Required";
					Assert.assertEquals(actual_msg, expected_msg);
				  }
				
				@Test (priority = 4)
				  public void EmailInvalid() throws InterruptedException {
					
					driver.navigate().refresh();
					driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/ul[2]/li[1]/a")).click();
					
					driver.findElement(By.id("Email")).sendKeys("pateldimple2323@gmail.com");
					driver.findElement(By.id("Password")).sendKeys("123456");
					driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/div/div/form/footer/button")).click();
					Thread.sleep(5000);
					
					String actual_msg= driver.findElement(By.xpath("/html/body/div[12]/div/div/div/div/div/div/div[2]")).getText();
					String expected_msg = "Invalid email or password.";
					Assert.assertEquals(actual_msg, expected_msg);
					
					driver.findElement(By.xpath("/html/body/div[12]/div/div/div/div/footer/button")).click();
					
					
				  }
				
				@Test (priority = 5)
				  public void PasswordInvalid() throws InterruptedException {
					
					driver.navigate().refresh();
					driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/ul[2]/li[1]/a")).click();
					
					driver.findElement(By.id("Email")).sendKeys("ankur1.manish@gmail.com");
					driver.findElement(By.id("Password")).sendKeys("123456@#@##@##");
					driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/div/div/form/footer/button")).click();
					Thread.sleep(3000);
					
					String actual_msg= driver.findElement(By.xpath("/html/body/div[12]/div/div/div/div/div/div/div[2]")).getText();
					String expected_msg = "Invalid email or password.";
					Assert.assertEquals(actual_msg, expected_msg);
					
					driver.findElement(By.xpath("/html/body/div[12]/div/div/div/div/footer/button")).click();
					
				  }
				
				@Test (priority = 6)
				  public void EmailPasswordvalid() throws InterruptedException {
					
					driver.navigate().refresh();
					driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/ul[2]/li[1]/a")).click();
					
					driver.findElement(By.id("Email")).sendKeys("ankur1.manish@gmail.com");
					driver.findElement(By.id("Password")).sendKeys("12345678");;
					driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/div/div/form/footer/button")).click();
					Thread.sleep(3000);
					
					
					
				  }
	
					
				
	  
	
 
  

  @AfterClass
  public void afterClass() {
	  
	  driver.close();
  }

}
