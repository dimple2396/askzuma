package Inheritance;

import org.openqa.selenium.*;

import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class Editprofilec1 extends Shopclues_Login_parentclass {
	
   //edit Profile
	@Test (priority = 0)
	public void FirstnameBlank() throws InterruptedException {
		
			

	driver.findElement(By.id("firstname")).clear();
	driver.findElement(By.id("save_profile_but")).click();
	Thread.sleep(10000);

	String actualmsg=driver.findElement(By.id("checkBlank1")).getText();
	String expectmsg="First name cannot be blank";
	Assert.assertEquals(actualmsg,expectmsg);
	 
	  }
	  @Test (priority = 1)
	  public void LastnameBlank() throws InterruptedException {
	 
	 driver.findElement(By.id("firstname")).sendKeys("dimple");;
	 
	driver.findElement(By.id("lastname")).clear();
	driver.findElement(By.id("save_profile_but")).click();
	Thread.sleep(10000);

	String actualmsg=driver.findElement(By.id("checkBlank2")).getText();
	String expectmsg="Last name cannot be blank";
	Assert.assertEquals(actualmsg,expectmsg);
	
	  }
	  @Test (priority = 2)
	  public void phoneBlank() throws InterruptedException {
	 
	 
	 driver.findElement(By.id("lastname")).sendKeys("patel");
	 driver.findElement(By.id("phone")).clear();
	 driver.findElement(By.id("save_profile_but")).click();
	 Thread.sleep(10000);

	String actualmsg=driver.findElement(By.id("checkBlank3")).getText();
	String expectmsg="Mobile number cannot be blank";
	Assert.assertEquals(actualmsg,expectmsg);
	
	  }
	  @Test (priority = 3)
	  public void AllvalidBlank() throws InterruptedException {
	 
	
	 driver.findElement(By.id("phone")).sendKeys("9512114619");;
	 driver.findElement(By.id("save_profile_but")).click();
	 Thread.sleep(10000);

	String actualmsg=driver.findElement(By.xpath("//*[@id=\"notification_85214fdf7d646b0d64eeb251383996d0\"]/div")).getText();
	String expectmsg="Profile has been updated successfully.";
	Assert.assertEquals(actualmsg,expectmsg);
	Thread.sleep(10000);

	
	  }
	 
	 
  
 

}
