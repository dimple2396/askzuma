package Inheritance;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;

import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class Addtocartc3 extends Changepasswordc2
{
	
	
  @Test (priority = 11)
  public void addtocart() throws InterruptedException {
	  
	  driver.get("https://www.shopclues.com/");
	  
	  JavascriptExecutor js= (JavascriptExecutor)driver;
		js.executeScript("scrollBy(0,800)");
		Thread.sleep(3000);
		Thread.sleep(3000);
		Thread.sleep(3000);
	  
	  //img click
	  driver.findElement(By.xpath("//*[@id=\"127779866\"]/img")).click();
		Thread.sleep(5000);
		
	//New Tab	
	ArrayList<String> tabs= new ArrayList<String>(driver.getWindowHandles());
	driver.switchTo().window(tabs.get(1));	
		
	JavascriptExecutor jsnew=(JavascriptExecutor)driver;
	jsnew.executeScript("scrollBy(0,400)");
	Thread.sleep(5000);
				
    driver.findElement(By.id("add_cart")).click();
		  
	Actions action=new Actions(driver);
	WebElement element=driver.findElement(By.className("cart_ic"));
	action.moveToElement(element).perform();
	Thread.sleep(5000);
			
	//view cart option
	driver.findElement(By.xpath("/html/body/div[3]/div/div/div[4]/ul/li[4]/div/div/div[3]/a[1]")).click();
	

	//verify price
	String actualmsg=driver.findElement(By.xpath("//*[@id=\"gt-cart-price\"]/p[1]")).getText();
	String expectmsg="Total\nRs 208";
	Assert.assertEquals(actualmsg,expectmsg);
	Thread.sleep(5000);	
	
	//plus item
	driver.findElement(By.xpath("//*[@id=\"127779866_2666290063\"]/div[2]/div[2]/span/a[2]")).click();
	Thread.sleep(5000);	
	
	//minus item
	driver.findElement(By.xpath("//*[@id=\"127779866_2666290063\"]/div[2]/div[2]/span/a[1]")).click();
	Thread.sleep(5000);	
	
    //remove item
	driver.findElement(By.xpath("//*[@id=\"127779866_2666290063\"]/div[2]/div[2]/a")).click();
	driver.findElement(By.xpath("//*[@id=\"loadContent\"]/div[1]/div[2]/div[2]/a[1]")).click();
	
	
	 
		
	  }
  
  
  
  
  
  
  
  @BeforeTest
  public void beforeTest() {
  }

  @AfterTest
  public void afterTest() {
  }

}
