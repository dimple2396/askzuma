package Inheritance;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class repair_child1 extends Askzuma_Signin_parentclass {
	
  @Test (priority = 7)
  public void Selectyear() throws InterruptedException {
	  
	  JavascriptExecutor js= (JavascriptExecutor)driver;
	  js.executeScript("scrollBy(0,400)");
	  Thread.sleep(5000);
	  
	  driver.findElement(By.id("select2-chosen-1")).click();
	  driver.findElement(By.id("select2-result-label-8")).click();
	  Thread.sleep(5000);

	  driver.findElement(By.id("select2-chosen-46")).click();
	  driver.findElement(By.id("select2-result-label-50")).click();
	  Thread.sleep(5000);

	  driver.findElement(By.id("select2-chosen-90")).click();
	  driver.findElement(By.id("select2-results-90")).click();
	  Thread.sleep(5000);

	  driver.findElement(By.id("select2-chosen-121")).click();
	  driver.findElement(By.id("select2-results-121")).click();
	  Thread.sleep(5000);

	  driver.findElement(By.id("selectJob")).click();

	  String actual=driver.findElement(By.className("message")).getText();
	  String expected="Please specify the the location.";

	  Assert.assertEquals(actual, expected);
	  Thread.sleep(5000);
	  driver.findElement(By.xpath("/html/body/div[12]/div/div/header/span")).click();
	    }
  
  
  
  @BeforeTest
  public void beforeTest() {
  }

  @AfterTest
  public void afterTest() {
  }

}
