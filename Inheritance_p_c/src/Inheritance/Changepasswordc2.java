package Inheritance;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class Changepasswordc2 extends Editprofilec1{
	
	
  @Test (priority = 4)
  public void Allblankfields() {
	  
	  //change password link click
	  driver.findElement(By.xpath("//*[@id=\"content\"]/div[1]/div[5]/div/div/div[2]/div/div/div/div[1]/ul/li[4]/a")).click();
	  
	  //all blank
	  driver.findElement(By.id("save_profile_but")).click();
	  
	  String Actual=driver.findElement(By.id("passwordBlank")).getText();
	  String Expected="Current password cannot be empty";
	  Assert.assertEquals(Actual, Expected);
  }
  
  @Test (priority = 5)
  public void CurrentPasswordenter() throws InterruptedException {
	  
	  driver.findElement(By.id("passwordc")).sendKeys("123456789632df");
	  driver.findElement(By.id("save_profile_but")).click();
	  Thread.sleep(5000);
	  
	  String actualmsg= driver.findElement(By.id("password1Blank")).getText();
	  String expectmsg="New password cannot be empty";
	  Assert.assertEquals(actualmsg, expectmsg);
 
  }
  
  @Test (priority = 6)
  public void NewPasswordenter() throws InterruptedException {
	  
	  //driver.findElement(By.id("passwordc")).sendKeys("123456789632df");
	  driver.findElement(By.id("password1")).sendKeys("123456a");
	  driver.findElement(By.id("save_profile_but")).click();
	  Thread.sleep(5000);
	  
	  String actualmsg= driver.findElement(By.id("password2Blank")).getText();
	  String expectmsg="Confirm password cannot be empty";
	  Assert.assertEquals(actualmsg, expectmsg);
 
  }
  
  
  
  
  @Test (priority = 7)
  public void ConfirmNewPasswordenter() throws InterruptedException {
	  
	  driver.findElement(By.id("password2")).sendKeys("123456a");
	  driver.findElement(By.id("save_profile_but")).click();
	  Thread.sleep(5000);
	  
	  String actualmsg= driver.findElement(By.className("notification-e")).getText();
	 // String actualmsg=driver.findElement(By.className("notification-header-e")).getText();
	  String expectmsg="Wrong ! Password please try again.";
	  Assert.assertEquals(actualmsg, expectmsg);
	  
  }
  
  
  
  @Test (priority = 8)
  public void Currentpassblank() throws InterruptedException {
	
	  driver.findElement(By.id("password1")).sendKeys("123456a");
	  driver.findElement(By.id("password2")).sendKeys("123456a");
	  driver.findElement(By.id("save_profile_but")).click();
	  Thread.sleep(5000);
	  
	  String actualmsg= driver.findElement(By.id("passwordBlank")).getText();
	  String expectmsg="Current password cannot be empty";
	  Assert.assertEquals(actualmsg, expectmsg);
 
}
  
  @Test (priority = 9)
  public void Newpassblank() throws InterruptedException {
	  driver.navigate().refresh();
	
	  driver.findElement(By.id("passwordc")).sendKeys("123456789632df");
	  driver.findElement(By.id("password2")).sendKeys("123456a");
	  driver.findElement(By.id("save_profile_but")).click();
	  Thread.sleep(5000);
	  
	  String actualmsg= driver.findElement(By.id("password1Blank")).getText();
	  String expectmsg="New password cannot be empty";
	  Assert.assertEquals(actualmsg, expectmsg);
 
}
 
  @Test (priority = 10)
  public void newconfirmdiff() throws InterruptedException {
	  
	  driver.navigate().refresh();
	  
	  driver.findElement(By.id("passwordc")).sendKeys("123456789632df");
	  driver.findElement(By.id("password1")).sendKeys("12342df");
	  driver.findElement(By.id("password2")).sendKeys("123456a");
	  driver.findElement(By.id("save_profile_but")).click();
	  Thread.sleep(5000);
	  
	  String actualmsg= driver.findElement(By.id("newEqualToConfirm")).getText();
	  String expectmsg="New password and confirm new password do not match";
	  Assert.assertEquals(actualmsg, expectmsg);
	  
	 
}
  

  
  
  
  
  @BeforeTest
  public void beforeTest() {
  }

  @AfterTest
  public void afterTest() {
  }

}
